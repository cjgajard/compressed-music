## Dependencies

You need a midi player, I used timidity:
```
sudo apt-get install freepats timidity timidity-interfaces-extra
```

## Testing

To hear it I run:
```
./decompress.py && timidity output.mid
```
